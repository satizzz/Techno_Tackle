<?php
    $message = "";
    $input1=$input2=$OutputString1=$OutputString2='';
    if(isset($_POST['SubmitButton'])){ //check if form was submitted
            $input1 = $_POST['string1']; //get input text
            $input2 = $_POST['string2']; //get input text
            $stringArray1=str_split($input1);
            $stringArray2=str_split($input2);

            for( $i=0;$i<count($stringArray1);$i++)
            {
                if($i % 2 == 0){
                    $OutputString1.=$stringArray1[$i];
                }
                else{
                    $OutputString2.=$stringArray1[$i];
                }
            }
            for( $k=0;$k<count($stringArray2);$k++)
            {
                if($k % 2 == 0){
                    $OutputString1.=$stringArray2[$k];
                }
                else{
                    $OutputString2.=$stringArray2[$k];
                }
            }
    }    
?>

<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>    
<div class="container"> 
    <hr>
    <div class="row">
        <form action="" method="post">
            <div class="form-group">
                <label for="usr">String 1:</label>
                <input type="text" name="string1" class="form-control" value="<?php echo $input1;?>">
            </div>
            <div class="form-group">
                <label for="usr">String 2:</label>
                <input type="text" name="string2" class="form-control"  value="<?php echo $input2;?>">
            </div>
            <button type="submit" name="SubmitButton" class="btn btn-primary">Submit</button>
        </form> 
    </div>
    
    <div class="container">
        <p> <h2><?php echo "output1 : " .$OutputString1.'<br>'; ?></h2></p>
        <p> <h2><?php echo "output2 : " .$OutputString2.'<br>'; ?></h2></p>
    </div>

</div>
   
</body>
</html>
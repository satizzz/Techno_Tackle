-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2023 at 08:08 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `Cid` int(11) NOT NULL,
  `Customer_Name` varchar(255) NOT NULL,
  `User_Name` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`Cid`, `Customer_Name`, `User_Name`, `Password`, `Email`, `Active`) VALUES
(1, 'Sathish', 'satiz', 'satiz', 'mail@gmail.com', 'Y'),
(2, 'kumar', 'kumar001', 'kumar001', 'kumar001@mail.com', 'Y'),
(3, 'dinesh', 'dinesh', 'dinesh', 'dinesh@mail.com', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE `customer_group` (
  `CG_id` int(11) NOT NULL,
  `Group_Name` varchar(255) NOT NULL,
  `Customers` text NOT NULL,
  `Status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `Oid` int(255) NOT NULL,
  `Order_id` varchar(255) NOT NULL,
  `Cid` int(11) NOT NULL,
  `Pid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Oid`, `Order_id`, `Cid`, `Pid`, `qty`, `DateCreated`) VALUES
(12, 'ORDER_1', 1, 3, 3, '2023-07-12 18:07:33'),
(13, 'ORDER_1', 1, 2, 31, '2023-07-12 18:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `Pid` int(11) NOT NULL,
  `Product_Name` varchar(255) NOT NULL,
  `Product_Image` blob NOT NULL,
  `Price` double NOT NULL,
  `Status` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Pid`, `Product_Name`, `Product_Image`, `Price`, `Status`) VALUES
(1, 'orange', '', 100, 'Y'),
(2, 'Apple', '', 150, 'Y'),
(3, 'Grape', '', 75.5, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `Sid` int(11) NOT NULL,
  `Pid` int(255) NOT NULL,
  `Stock` int(11) NOT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`Sid`, `Pid`, `Stock`, `DateCreated`) VALUES
(1, 1, 1000, '2023-07-11 17:15:50'),
(2, 3, 500, '2023-07-11 17:15:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`Cid`),
  ADD UNIQUE KEY `User_Name` (`User_Name`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Oid`),
  ADD KEY `Pid` (`Pid`),
  ADD KEY `Cid` (`Cid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`Pid`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD KEY `Pid` (`Pid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `Cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `Oid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `Pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`Pid`) REFERENCES `products` (`Pid`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`Cid`) REFERENCES `customers` (`Cid`);

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`Pid`) REFERENCES `products` (`Pid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
